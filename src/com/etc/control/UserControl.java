package com.etc.control;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.etc.entity.Users;
import com.etc.srevice.UsersService;
import com.etc.util.CommonMessage;
import com.etc.util.PageData;

@Controller
@RequestMapping("api")
public class UserControl {
	@Resource(name="userService")
	private UsersService us;
	/**
	 * 
	 * @return 跳转到分页的页面
	 */
	@RequestMapping(value="user")
	public String user() {
		return "userLayUI";
	}
	/**
	 * 
	 * @param page 第几页，jsp传过来
	 * @param limit 页面显示的条数，jsp传过来
	 * @param content 模糊查询的内容
	 * @return
	 */
	@RequestMapping(value="userPage")
	@ResponseBody
	public PageData<Users> UserByPage(Integer page,Integer limit,String content){
		if(content==null)
	    content="";
		PageData<Users> pd=us.getUsersByPage(page, limit, content);
		pd.setCode(0);
		pd.setMsg("ok");
		return pd;
	}
	
	 
	@RequestMapping(value="edit")
	@ResponseBody
	public CommonMessage UserEdit(Users user){
		boolean flag=us.update(user);
		CommonMessage cm=new CommonMessage();
		if(flag) {
			cm.setMsg("修改成功");
			}else {
		cm.setMsg("修改失败");}
			return cm;
		
	}
	
	@RequestMapping(value="del")
	@ResponseBody
	public CommonMessage del(int userId){
	boolean flag=us.delUsers(userId);
	CommonMessage cm=new CommonMessage();
	if(flag) {
		cm.setMsg("删除成功");
		}else {
	cm.setMsg("删除失败");}
		return cm;
	}
	
	@RequestMapping(value="add")
	@ResponseBody
	public CommonMessage UserAdd(Users user){
		boolean flag=us.addUser(user);
		CommonMessage cm=new CommonMessage();
		if(flag) {
			cm.setMsg("增加成功");
			}else {
		cm.setMsg("增加失败");}
			return cm;
		
	}

}
