<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>layui</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet" href="../layui/css/layui.css" media="all">
<!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<body>
<div id="addForm" style="display: none">
		<form class="layui-form" action="" lay-filter="example">
			<div class="layui-form-item">
				<label class="layui-form-label">用户名</label>
				<div class="layui-input-block">
					<input type="text" name="userName" id="userName1" lay-verify="title"
						autocomplete="off" placeholder="请输入用户名" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">密码</label>
				<div class="layui-input-block">
					<input type="password" name="userPwd" id="userPwd" lay-verify="title"
						autocomplete="off" placeholder="" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">电话</label>
				<div class="layui-input-block">
					<input type="text" name="userTel" id="userTel1" lay-verify="title"
						autocomplete="off" placeholder="请输入电话" class="layui-input">
				</div>
			</div>

			<div class="layui-form-item">
				<label class="layui-form-label">性别</label>
				<div class="layui-input-block">
					<input type="radio" name="userSex" value="男" title="男" checked="">
					<input type="radio" name="userSex" value="女" title="女">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">状态</label>
				<div class="layui-input-block">
					<input type="radio" name="userState" value=0 title="禁用" checked="">
					<input type="radio" name="userState" value=1 title="启用">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">等级</label>
				<div class="layui-input-block">
					<input type="radio" name="userLevel" value=0 title="管理员" checked="">
					<input type="radio" name="userLevel" value=1 title="用户">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">地址</label>
				<div class="layui-input-block">
					<input type="text" name="userAddress" id="userAddress1"
						lay-verify="title" autocomplete="off" placeholder="请输入电话"
						class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-input-block">
					<button class="layui-btn" lay-submit="" lay-filter="add">立即提交</button>
				</div>
			</div>
		</form>
	</div>
	<div id="editForm" style="display: none">
		<form class="layui-form" action="" lay-filter="example">
			<div class="layui-form-item">
				<label class="layui-form-label">ID</label>
				<div class="layui-input-block">
					<input type="text" name="userId" id="userId" lay-verify="title"
						autocomplete="off" placeholder="" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">用户名</label>
				<div class="layui-input-block">
					<input type="text" name="userName" id="userName" lay-verify="title"
						autocomplete="off" placeholder="请输入用户名" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">电话</label>
				<div class="layui-input-block">
					<input type="text" name="userTel" id="userTel" lay-verify="title"
						autocomplete="off" placeholder="请输入电话" class="layui-input">
				</div>
			</div>

			<div class="layui-form-item">
				<label class="layui-form-label">性别</label>
				<div class="layui-input-block">
					<input type="radio" name="userSex" value="男" title="男" checked="">
					<input type="radio" name="userSex" value="女" title="女">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">状态</label>
				<div class="layui-input-block">
					<input type="radio" name="userState" value=0 title="禁用" checked="">
					<input type="radio" name="userState" value=1 title="启用">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">等级</label>
				<div class="layui-input-block">
					<input type="radio" name="userLevel" value=0 title="管理员" checked="">
					<input type="radio" name="userLevel" value=1 title="用户">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">地址</label>
				<div class="layui-input-block">
					<input type="text" name="userAddress" id="userAddress"
						lay-verify="title" autocomplete="off" placeholder="请输入电话"
						class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-input-block">
					<button class="layui-btn" lay-submit="" lay-filter="demo1">立即提交</button>
				</div>
			</div>
		</form>
	</div>
	<div class="demoTable">
		搜索名字：
		<div class="layui-inline">
			<input class="layui-input" name="id" id="demoReload"
				autocomplete="off">
		</div>
		<button class="layui-btn" data-type="reload">搜索</button>
	</div>

	<table class="layui-hide" id="test" lay-filter="test"></table>

	<script type="text/html" id="toolbarDemo">
  <div class="layui-btn-container">
    <button class="layui-btn layui-btn-sm" lay-event="getCheckData">获取选中行数据</button>
    <button class="layui-btn layui-btn-sm" lay-event="getCheckLength">获取选中数目</button>
    <button class="layui-btn layui-btn-sm" lay-event="isAll">验证是否全选</button>
    <button class="layui-btn layui-btn-sm" lay-event="addUser">增加</button>
  </div>
</script>

	<script type="text/html" id="barDemo">
  <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
  <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>


	<script src="../layui/layui.js" charset="utf-8"></script>
	<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->

	<script>
		layui.use('table', function my() {
			var table = layui.table;

			table.render({
				elem : '#test',
				url : 'userPage',
				toolbar : '#toolbarDemo',
				height : 550,
				title : '用户数据表',
				cols : [ [ {
					type : 'checkbox',
					fixed : 'left'
				}, {
					field : 'userId',
					title : 'ID',
					width : 80,
					fixed : 'left',
					unresize : true,
					sort : true
				}, {
					field : 'userName',
					title : '用户名',
					width : 120,
				}, {
					field : 'email',
					title : '邮箱',
					width : 150,
					edit : 'text',
					templet : function(res) {
						return '<em>' + res.email + '</em>'
					}
				}, {
					field : 'userPwd',
					title : '密码',
					width : 80,
					sort : true
				}, {
					field : 'userSex',
					title : '性别',
					width : 100
				}, {
					field : 'userTel',
					title : '电话',
					width : 80
				}, {
					field : 'userAddress',
					title : '地址',
					width : 150,
					sort : true
				}, {
					field : 'userLevel',
					title : '等级',
					width : 120,
					templet : function(res) {
						return res.userLevel == 0 ? "管理员" : "用户";
					}
				}, {
					field : 'userState',
					title : '状态',
					width : 100,
					sort : true,
					templet : function(res) {
						return res.userState == 0 ? "禁用" : "启用";
					}
				}, {
					fixed : 'right',
					title : '操作',
					toolbar : '#barDemo',
					width : 150
				} ] ],
				page : true,
				id : 'testReload'
			});

			//头工具栏事件
			table.on('toolbar(test)', function(obj) {
				var checkStatus = table.checkStatus(obj.config.id);
				switch (obj.event) {
				case 'getCheckData':
					var data = checkStatus.data;
					layer.alert(JSON.stringify(data));
					break;
				case 'getCheckLength':
					var data = checkStatus.data;
					layer.msg('选中了：' + data.length + ' 个');
					break;
				case 'isAll':
					layer.msg(checkStatus.isAll ? '全选' : '未全选');
					break;
			case 'addUser':
				
					var cx=layer.open({
						type : 1,
						content : $("#addForm").html(),
					});
					//只有加了这一句，表单的复选框，单选框才可以编辑
					layui.form.render();
					//监听提交
					layui.form.on('submit(add)', function(data2) {
						var d = data2.field;
						console.log(d)
						$.ajax({
							type : "get",
							url : "add",
							data : d,
							success : function(data) {
								if (data.msg == "增加成功") {
									layer.msg(data.msg, {
										time : 1000
									}, function() {
										layer.close(cx);
										my();
									});

								} else {
									layer.msg(data.msg)
								}
							}
						});

						return false;
					});

				
				break;
			}
				;
			});
		//监听行工具事件
			table.on('tool(test)', function(obj) {

				var data = obj.data;
				if (obj.event === 'del') {
					layer.confirm('真的删除行么', function(index) {
						$.ajax({
							type : "get",
							url : "del",
							data : {
								userId : data.userId
							},
							success : function(data) {
								console.log(data.msg);
								if (data.msg == "删除成功") {
									layer.msg(data.msg, {
										time : 1000
									}, function() {
										obj.del();
										layer.close(index1);
										my();
									});

								} else {
									layer.msg(data.msg)
								}
							}
						});

					});
				} else if (obj.event === 'edit') {
					$("#userId").attr("value", data.userId);
					$("#userName").attr("value", data.userName);
					// 					$("#userPwd").attr("value", data.userPwd);
					$("#userTel").attr("value", data.userTel);
					$("#userAddress").attr("value", data.userAddress);
					$("input[name='userSex'][value='男']").attr("checked",
							data.userSex == "男" ? true : false);
					$("input[name='userSex'][value='女']").attr("checked",
							data.userSex == "女" ? true : false);
					$("input[name='userState'][value=0]").attr("checked",
							data.userState == 0 ? true : false);
					$("input[name='userState'][value=1]").attr("checked",
							data.userState == 1 ? true : false);
					$("input[name='userLevel'][value=0]").attr("checked",
							data.userLevel == 0 ? true : false);
					$("input[name='userLevel'][value=1]").attr("checked",
							data.userLevel == 1 ? true : false);
					var index1 = layer.open({
						type : 1,
						content : $("#editForm").html(),
					});
					//只有加了这一句，表单的复选框，单选框才可以编辑
					layui.form.render();
					//监听提交
					layui.form.on('submit(demo1)', function(data2) {
						var d = data2.field;
						console.log(d)
						$.ajax({
							type : "get",
							url : "edit",
							data : d,
							success : function(data) {
								if (data.msg == "修改成功") {
									layer.msg(data.msg, {
										time : 1000
									}, function() {
										layer.close(index1);
										my();
									});

								} else {
									layer.msg(data.msg)
								}
							}
						});

						return false;
					});
				}
			});

			var $ = layui.$, active = {
				reload : function() {
					var demoReload = $('#demoReload');

					//执行重载
					table.reload('testReload', {
						page : {
							curr : 1
						//重新从第 1 页开始
						},
						where : {
							// 		          key: {
							// 		            content: demoReload.val()
							// 		          }
							content : demoReload.val()
						}
					});
				}
			};

			$('.demoTable .layui-btn').on('click', function() {
				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
			});
		});
	</script>

</body>
</html>