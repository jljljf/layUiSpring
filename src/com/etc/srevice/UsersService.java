package com.etc.srevice;

import java.util.List;

import com.etc.entity.Users;
import com.etc.util.PageData;

public interface UsersService {
	public Users getUserById(int userId);
	public List<Users> getUsers();
	public boolean addUser(Users user);
	public PageData<Users> getUsersByPage(int page, int pageSize, String content);
	public boolean delUsers(int userId);
	public boolean update(Users user);
}
