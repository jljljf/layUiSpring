package com.etc.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.etc.dao.UsersDao;
import com.etc.entity.UserRowMapper;
import com.etc.entity.Users;
import com.etc.util.PageData;

@Repository(value = "usersDao")
public class UsersDaoImpl implements UsersDao {
	@Resource(name = "jdbcTemplate")
	private JdbcTemplate jdbc;

	@Override
	public Users getUserById(int userId) {
		// TODO Auto-generated method stub
		String sql = "select * from user where userId=?";
		return jdbc.queryForObject(sql, new UserRowMapper(), userId);

	}

	@Override
	public List<Users> getUsers() {
		// TODO Auto-generated method stub
		String sql = "select * from user";
		return jdbc.query(sql, new UserRowMapper());

	}

	@Override
	public boolean addUser(Users user) {
		// TODO Auto-generated method stub
		String sql = "INSERT INTO `user` (`userName`, `userPwd`, `userSex`, `userTel`, `userAddress`, `userLevel`, `userState`) VALUES (?, ?, ?, ?,?, ?, ?)";
		return jdbc.update(sql, user.getUserName(), user.getUserPwd(), user.getUserSex(), user.getUserTel(),
				user.getUserAddress(), user.getUserLevel(), user.getUserState())>0;
	}

	@Override
	public List<Users> getUsersByPage(int start, int limit, String content) {
		String sql = "select * from user where userName like ?  LIMIT ?,?";
		return jdbc.query(sql, new UserRowMapper(), "%" + content + "%", start, limit);

	}

	@Override
	public int getTotal(String content) {
		String sql = "select COUNT(userId) from user where userName like ?";
		return jdbc.queryForObject(sql, Integer.class, "%" + content + "%");
	}

	@Override
	public boolean delUsers(int userId) {
		String sql="delete from user where userId=?";
		return jdbc.update(sql,userId )>0;
	}

	@Override
	public boolean update(Users user) {
		String sql="UPDATE user set userName=?, userSex=?,userTel=?,userAddress=?,userLevel=?,userState=? where userId=?";

		return jdbc.update(sql,user.getUserName(),user.getUserSex(),user.getUserTel(),user.getUserAddress(),user.getUserLevel(),user.getUserState(),user.getUserId())>0;
	}

}
