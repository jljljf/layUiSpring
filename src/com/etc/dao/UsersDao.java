package com.etc.dao;

import java.util.List;

import com.etc.entity.Users;
import com.etc.util.PageData;

public interface UsersDao {
	public Users getUserById(int userId);
	public List<Users> getUsers();
	public boolean addUser(Users user);
	public List<Users> getUsersByPage(int start, int pageSize, String content);
	public int getTotal(String content);

	/**
	 * 删除功能
	 * @param userId
	 * @return
	 */
	public boolean delUsers(int userId);
	
	public boolean update(Users user);
}
