package com.etc.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.catalina.User;
import org.springframework.jdbc.core.RowMapper;

public class UserRowMapper implements RowMapper<Users> {

	@Override
	public Users mapRow(ResultSet rs, int arg1) throws SQLException {
		// TODO Auto-generated method stub
		Users users=new Users();
		users.setUserAddress(rs.getString("userAddress"));
		users.setUserId(rs.getInt("userId"));
		users.setUserLevel(rs.getInt("userLevel"));
		users.setUserName(rs.getString("userName"));
		users.setUserPwd(rs.getString("userPwd"));
		users.setUserSex(rs.getString("userSex"));
		users.setUserState(rs.getInt("userState"));
		users.setUserTel(rs.getString("userTel"));
		return users;
	}

}
