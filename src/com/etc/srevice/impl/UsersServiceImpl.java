package com.etc.srevice.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.etc.dao.UsersDao;
import com.etc.entity.Users;
import com.etc.srevice.UsersService;
import com.etc.util.PageData;
@Service(value="userService")
public class UsersServiceImpl implements UsersService {
	@Resource(name="usersDao")
	private UsersDao userDao;

	@Override
	public Users getUserById(int userId) {
		// TODO Auto-generated method stub
		System.out.println(userDao.getUserById(userId));
		return userDao.getUserById(userId);
	}

	@Override
	public List<Users> getUsers() {
		// TODO Auto-generated method stub
		return userDao.getUsers();
	}

	@Override
	public boolean addUser(Users user) {
		// TODO Auto-generated method stub
		return userDao.addUser(user);
	}

	@Override
	public PageData<Users> getUsersByPage(int page, int limit, String content) {
		// TODO Auto-generated method stub
		int start=(page-1)*limit;
		List<Users> data=userDao.getUsersByPage(start, limit, content);
		int count=userDao.getTotal(content);
		PageData<Users> pd=new PageData<>(data, count, limit, page);
		return pd;
	}

	@Override
	public boolean delUsers(int userId) {
		// TODO Auto-generated method stub
		
		return userDao.delUsers(userId);
	}

	@Override
	public boolean update(Users user) {
		// TODO Auto-generated method stub
		return userDao.update(user);
	}


}
